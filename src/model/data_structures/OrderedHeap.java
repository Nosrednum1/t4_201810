package model.data_structures;

import Api.IHeapArray;

public class OrderedHeap<T extends Comparable<T>> implements IHeapArray<T>{

	private T[] data;
	private int size;

	@SuppressWarnings("unchecked")
	public OrderedHeap(int capacity) {data = (T[]) new Comparable[capacity+1];}

	/**
	 * 	Take the array and make a new array whit size + 1/10(size)
	 * @param Array array to expand
	 * @return new array, with all elements of the "Array"
	 */
	public void expand() {
		T[] newArray = (T[]) new Comparable[data.length + (int) Math.ceil((data.length + 1)/8)];
		for (int i = 1; i < data.length; i++) {
			newArray[i] = data[i];
		}
		data = newArray;
	}

	@Override
	public void insert(T element) {
		if (size == data.length - 1)
			expand();

		size++;
		data[size] = element;
		reorderUp();
	}

	private void reorderUp() {
		int index = size();
		while (index > 1){
			int parent = index / 2;
			if (less(index, parent))
				//break if the parent is greater or equal to the current element
				break;
			exchange(index,parent);
			index = parent;
		}  
	}
	private void reordeDown() {
		int index = 1;
		while (true){
			int child = index*2;
			if (child > size())
				break;
			if (child + 1 <= size()){
				//if there are two children -> take the smalles or
				//if they are equal take the left one
				child = findMin(child, child + 1);
			}
			if (!less(index, child))
				break;
			exchange(index,child);
			index = child;
		}
	}

	private int findMin(int leftChild, int rightChild) {
		if (compare(data[leftChild], data[rightChild]) >= 0)
			return leftChild;
		else
			return rightChild;
	}

	@Override
	public T remove() {
		if (size != 0) {
			T min = data[1];
			data[1] = data[size];
			size--;
			reordeDown();
			return min;
		}
		return null;
	}

	@Override
	public int size(){return size;}

	public String toString() {
		String ret = "";
		for (T t : data) {
			ret += t + " ";
		}
		return ret;
	}

	private int compare(Comparable<T> x, Comparable<T> y) {
		return x.compareTo( (T) y);
	}

	public T get(T element) {
		T t = null;
		for (int i = 1; i < size && t == null; i++) {
			if(data[i].compareTo(element) == 0) t = data[i];
		}
		return t;
	}

	@Override
	public boolean less(int i, int j) {
		return (data[i]!= null && data[j] != null)? data[i].compareTo(data[j]) < 0:false;
	}

	@Override
	public void exchange(int i, int j) {
		T t = data[i];
		data[i] = data[j];
		data[j] = t;
	}	
}
